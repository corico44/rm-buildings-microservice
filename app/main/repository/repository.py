from app.main.config import db

# Get collection "users"
building_collection = db["buildings"]


def get_all_buildings():
    return building_collection.find({})
