from flask_restx import Resource, Namespace

from ..service.service import get_all_buildings

api = Namespace('building', description='building related operations')


@api.route('')
@api.response(404, 'Building not found')
class GetAllBuildings(Resource):
    @api.doc('Get all buildings')
    def get(self):
        """get all buildings"""
        building = get_all_buildings()
        if not building:
            api.abort(404)
        else:
            return building
