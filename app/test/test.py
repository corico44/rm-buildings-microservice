from locust import HttpUser, task, between


class WebsiteTestUser(HttpUser):
    wait_time = between(0.5, 5.0)

    def on_start(self):
        pass

    def on_stop(self):
        pass


@task(1)
def get_all_buildings(self):
    self.client.get("http://reus-modernista-buildings.herokuapp.com/buildings")
