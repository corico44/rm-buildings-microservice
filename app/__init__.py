from flask import Blueprint
from flask_restx import Api
from .main.controller.controller import api as building_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='REUS MODERNISTA',
          version='1.0',
          description='flask rest web service for reus modernista'
          )

api.add_namespace(building_ns, path='/buildings')
