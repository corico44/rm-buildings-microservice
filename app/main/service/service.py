from app.main.repository import repository


def get_all_buildings():
    building_list = []
    buildings = repository.get_all_buildings()
    for building in buildings:
        building['_id'] = str(building['_id'])
        building_list.append(building)
    return building_list
